#!/bin/bash

# by Eriberto
# Create the manpage using txt2man command.

T2M_DATE="04 Dec 2022"
T2M_NAME=iwatch
T2M_VERSION=0.2.2
T2M_LEVEL=1
T2M_DESC="realtime filesystem monitoring program using inotify"

# Don't change the following line
txt2man -d "$T2M_DATE" -t $T2M_NAME -r $T2M_NAME-$T2M_VERSION -s $T2M_LEVEL -v "$T2M_DESC" $T2M_NAME.txt > $T2M_NAME.$T2M_LEVEL
